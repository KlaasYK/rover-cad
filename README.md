# Rover CAD

CAD designs for my Mars rover.

![Swivel Wheel](./parts/prototype/images/swivel_wheel.png)

## Usage

Most designs are made with [FreeCAD](https://www.freecad.org/), though some parts are only available as a `.step` files (e.g. generated 3D models of PCB's from KiCAD).

## Support

**TODO** link to blog

## Roadmap

**TODO** link to blog

## Project status & contributing

This is currently a hobby project solely developped by me. Feel free to provide feedback, but I am not actively accepting contributions at this time.

## Authors and acknowledgment

**TODO** link to blog

## License
Unless otherwise stated [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)


