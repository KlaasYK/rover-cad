# Parts used in the rover prototype

These are all "rough" (probably overengineered) parts used during prototyping my rover.

## Available Parts

### ESC Mount

For mounting a brushless speed controller (look for `ZS-X11H v1` on your favourite asian marketplace). **TODO** Describe the screws used.

![ESC mount](./images/esc_mount.png)

### Arduino + rover HAT mount

Main board mount for the Arduino Mega and a custom HAT PCB. **TODO** Add link to the KiCAD design.

**NOTE:** uses a negative volume for use in the slicer (shown in red).

![Main board mount](./images/main_board.png)

### Steering mount

Swivel mount for the wheels, using stepper motors for turning.

![Swivel mount](./images/swivel_wheel.png)