# TD788R RJ45 (UTP or 8P8C) PCB Connector

Retailers:
- Sparkfun: https://www.sparkfun.com/products/643
- Okaphone: https://www.okaphone.com/artikel.asp?id=487411

CAD drawing based on: https://cdn.sparkfun.com/datasheets/Prototyping/04908.pdf
